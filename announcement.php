<?php
	include('config.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>OFFICIALS WEBSITE OF ENRILE CAGAYAN</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
<link href="assets/css/docs.css" rel="stylesheet">
<link href="assets/css/prettyPhoto.css" rel="stylesheet" type="text/css">
<link href="assets/js/google-code-prettify/prettify.css" rel="stylesheet">
<!--[if lt IE 9]>
<script src="assets/js/html5.js"></script>
<![endif]-->
<link rel="shortcut icon" href="assets/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
</head>
<body data-spy="scroll" data-target=".bs-docs-sidebar">
<div class="nav-agency">
  <div class="navbar navbar-static-top">
    <div class="navbar-inner">
      <div class="container"> <a class="brand" href="index.php"> <img src="imgs/logo2.png" alt=""></a>
        <div id="main-nav">
          <div class="nav-collapse collapse">
            <ul class="nav">
            <li class="active"><a href="index.php">HOME</a> </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"> FOR RESIDENT <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                   
					<li><a href="history.php">History</a></li>
					<li><a href="vision.php">Vision</a></li>
					<li><a href="mission.php">Mission</a></li>
					 <li><a href="news.php">News</a></li>
                    <li><a href="activity.php"> Activities</a></li>
                    <li><a href="gallery.php">Gallery</a></li>
					 <li><a href="baranggay.php">Barangay</a></li>
					
                  </ul>
                </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"> FOR VISITOR<b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="festival.php">Festival</a></li>
                    <li><a href="gallery.php">Destination</a></li>
                  </ul>
                </li>
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"> OFFICIALS <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="#">ELected Officials</a></li>
                     <li><a href="#">DepartmentHead</a></li>
                  </ul>
                </li>
                
                <li><a href="contact.php">CONTACT</a> </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="row-fluid">
    <div class="span12">
      <div class="page-header">
        <div class="row-fluid">
          <div class="span12">
            <h1> ANNOUNCEMENT</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="work">
    <div class="row-fluid">
      <div class="span4">
        <h5> </h5>
        <p></p>
        <h5></h5>
        <p></p>
							</p> <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Details</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
									$sqlemp=mysqli_query($dbcon,"select * from tblannouncements");
									while($rows=mysqli_fetch_array($sqlemp)){
								?>
                                    <tr>
                                       <td><?php echo $rows[1];?></td>
                                       <td><?php echo $rows[2];?></td>
                                       <td><?php echo $rows[3];?></td>
                                       <td><?php echo $rows[4];?></td>
                                      
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->




 </div>
    </div>
	      </div>
    </div>
      </div>
    </div>
<footer class="footer">
  <div class="container">
    <div class="row-fluid">
      <div class="span12">
        <blockquote>
          <p class="testimonial">Nalolohon Nu Pavvurulun !!!!!! BAYAN NG ENRILE</p>
          <p class="name">ADMIN</b></p>
        </blockquote>
      </div>
    </div>
    <hr class="soften1">
    <div class="row-fluid">
      <div class="span3">
        <h4>Navigation</h4>
        <ul class="footer-links">
          <li><a href="index.php">Home</a></li>
          <li><a href="news.php">NEWS</a></li>
          <li><a href="history.php">HISTORY</a></li>
          <li><a href="vision.php">VISION</a></li>
          <li><a href="mission.php">MISSION</a></li>
        </ul>
      </div>
      <div class="span3 MT70">
        <h4>Something from Flickr</h4>
        <div id="flickr-wrapper">
          <script src="http://www.flickr.com/badge_code_v2.gne?count=8&amp;display=latest&amp;size=s&amp;layout=x&amp;source=user&amp;user=10133335@N08"></script>
        </div>
      </div>
      <div class="span3 MT70">
        <h4>Who We Are</h4>
        <p>NALOLOHON NU PAVVURULUN</p>
        <ul class="footer_social clearfix">
          <li><a href="#" class="footer_facebook">Facebook</a></li>
          <li><a href="#" class="footer_twitter">Twitter</a></li>
          <li><a href="#" class="footer_googleplus">Google+</a></li>
          <li><a href="#" class="footer_rss">RSS</a></li>
        </ul>
      </div>
    </div>
    <hr class="soften1 copyhr">
    <div class="row-fluid copyright">
      <div class="span12">Copyright CSU WEB DEVELOPER</div>
    </div>
  </div>
</footer>
<script src="http://platform.twitter.com/widgets.js"></script>   
<script src="assets/js/jquery.js"></script>
<script src="assets/js/google-code-prettify/prettify.js"></script>
<script src="assets/js/bootstrap-transition.js"></script>
<script src="assets/js/bootstrap-alert.js"></script>
<script src="assets/js/bootstrap-modal.js"></script>
<script src="assets/js/bootstrap-dropdown.js"></script>
<script src="assets/js/bootstrap-scrollspy.js"></script>
<script src="assets/js/bootstrap-tab.js"></script>
<script src="assets/js/bootstrap-tooltip.js"></script>
<script src="assets/js/bootstrap-popover.js"></script>
<script src="assets/js/bootstrap-button.js"></script>
<script src="assets/js/bootstrap-collapse.js"></script>
<script src="assets/js/bootstrap-carousel.js"></script>
<script src="assets/js/bootstrap-typeahead.js"></script>
<script src="assets/js/bootstrap-affix.js"></script>
<script src="assets/js/application.js"></script>
<script src="assets/js/jquery.isotope.min.js"></script>
<script src="assets/js/jquery.prettyPhoto.js"></script>
<script src="assets/js/superfish.js"></script>
<script src="assets/js/custom.js"></script>
</body>
</html>